<?php

//This file contains functions used throughout various php scripts
// More comments need to be added

function secure_password($password){
    $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
    $salt = sprintf("$2a$%02d$", 10) . $salt;

    return crypt($password, $salt);
}
