<?php
/**
*
* Author: Josh Richard
* Date: 2014-05-25
*
* This file is used to declare variables for db handling
* or other secure information used in the php scripts
*
*/

$dbHost = 'localhost';
$dbUser = 'thesav1_infreg';
$dbPass = 'L1stM32014';
$dbName = 'thesav1_infreg';

// These are the keys and secrets for the Factual DB - the barcode db we use (20140525)
$key = "O76uijKv5qYz4qKipnPWm3zMD6LvBygNzDqACfel";
$secret = "DI0Mb1fWOfKWL35MZ66U0spnJdIR7RipNV9oPzy7";

$dbh = new mysqli($dbHost, $dbUser, $dbPass, $dbName);

if (mysqli_connect_errno()) {
	//use email function to email support@theinfiniteregistry.com
	echo "CONNECT ERROR!!!<br/>".$dbh->connect_errno."<br/>".$dbh->connect_error."<br/>";
    die('Connect Error (' . $dbh->connect_errno . ') '. $dbh->connect_error);
}elseif(!isset($dbh)){
	//use email function to email support@theinfiniteregistry.com
	echo "DBH not set <br/>".$dbh->connect_errno."<br/>".$dbh->connect_error."<br/>";
    die('Connect Error (' . $dbh->connect_errno . ') '. $dbh->connect_error);	
}
