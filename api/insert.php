<?php
include '../includes/db.php' ;

if(isset($_GET['type']))
	$table = escape($_GET['type']);
if(isset($_GET['uid']))
	$uid = escape($_GET['uid']);
else
	$uid = "0";
	
//Used for users
if(isset($_GET['user']))
	$username = escape($_GET['user']);
if(isset($_GET['pass']))
	$pass = escape($_GET['pass']);
if(isset($_GET['email']))
	$email = escape($_GET['email']);

//Used for lists
if(isset($_GET['name']))
	$name = escape($_GET['name']);
if(isset($_GET['fname']))
	$fname = escape($_GET['fname']);
if(isset($_GET['lname']))
	$lname = escape($_GET['lname']);
if(isset($_GET['desc']))
	$desc = escape($_GET['desc']);
if(isset($_GET['loc']))
	$loc = escape($_GET['loc']);

//Used for list_items
if(isset($_GET['listid']))
	$listid = escape($_GET['listid']);
else
	$listid = "0";
if(isset($_GET['itemid']))
	$itemid = escape($_GET['itemid']);
if(isset($_GET['qty']))
	$qty = escape($_GET['qty']);
	
$return = "No return";
function escape($string){
	global $dbh;
	return $dbh->escape_string($string);
}

function checkDbh(){
	global $dbh;
	if(!isset($dbh)){
		//use email function to email support@theinfiniteregistry.com
		echo "DBH not set <br/>";	
	}else{
		echo "dbh set <br/>";
	}
}

function getUid(){
	global $dbh, $uid, $username, $pass, $email;
	$query = "SELECT user_id FROM users WHERE username=? and pass=? and email=?";
	if(isset($dbh) && ($stmt = $dbh->prepare($query))){
		$stmt->bind_param('sss', $username, $pass, $email);
		$stmt->execute();
		$stmt->bind_result($uid);
		$stmt->fetch();
	    $stmt->free_result();
		$stmt->error;
		$stmt->close();
	}else{
		//write email function to email support@theinfiniteregistry.com the error
		echo "unable to prepare stmt getUID()<br/>";
	}
}

// parameter - existing: used to tell the function whether we are looking to find an existing list (true) or newly created (false)
function getListid($existing=true){
	global $dbh, $uid, $name, $listid;
	if($existing){
		echo "Checking for an existing list<br/>";
		$query = "SELECT list.list_id FROM lists list, lists_users user WHERE list.list_name=? and list.list_id=user.list_id and user.user_id=?";
		if(isset($dbh) && ($stmt = $dbh->prepare($query))){
			$stmt->bind_param('ss', $name, $uid);
			$stmt->execute();
			$stmt->bind_result($listid);
			$stmt->fetch();
		    $stmt->free_result();
			echo $stmt->error;
			$stmt->close();
		}else{
			echo $stmt->error;
			echo "<br/>".$dbh->error;
			//write email function to email support@theinfiniteregistry.com the error
			echo "<br/>unable to prepare stmt getListid()<br/>";
		}
		echo "End check<br/>";
	}else{
		echo "Checking for a new list<br/>";
		$query = "SELECT list_id FROM lists WHERE list_name=? and for_fname=? and for_lname=? and location=?";
		if(isset($dbh) && ($stmt = $dbh->prepare($query))){
			$stmt->bind_param('ssss', $name, $fname, $lname, $loc);
			$stmt->execute();
			$stmt->bind_result($listid);
			$stmt->fetch();
		    $stmt->free_result();
			echo $stmt->error;
			$stmt->close();
		}else{
			echo $stmt->error;
			echo "<br/>".$dbh->error;
			//write email function to email support@theinfiniteregistry.com the error
			echo "<br/>unable to prepare stmt getListid()<br/>";
		}
		echo "End check <br/>";
	}
	echo "List ID set: $listid<br/>";
}

switch ($table){
	case "user":
		getUid();
		if($uid == "0" || $uid == 0){
			$query = "INSERT INTO users (username, email, pass) VALUES(?, ?, ?)";
			if($stmt = $dbh->prepare($query)){
				$stmt->bind_param('sss', $username, $email, $pass);
				$stmt->execute();
				if($stmt->error != ""){
					$error_msg = $stmt->error;
					if(strpos($error_msg, "email") > -1){
						$arr = array('error'=>'email');
					}elseif(strpos($error_msg, "username") > -1){
						$arr = array('error'=>'username');
					}else{
						echo $error_msg;
					}
				}else{
					getUid();
					$arr = array('uid'=>$uid);
					$stmt->close();					
				}
			}else{
				//write email function to email support@theinfiniteregistry.com the error
				echo "unable to prepare stmt<br/>";
			}
		}else{
			$arr = array('uid'=>$uid);
		}
		
		$dbh->close();
		break;
	case "scan":
		$query = "INSERT INTO lists_items (list_id, item_id, qty) VALUES (?, ?, ?)";
		if($stmt = $dbh->prepare($query)){
			$stmt->bind_param('sss', $listid, $itemid, $qty);
			$stmt->execute();
			$stmt->close();
		}else{
			//write email function to email support@theinfiniteregistry.com the error
			echo "unable to prepare stmt<br/>";
		}
		break;
	case "list":
		getListid();
		if($listid == "0" || $listid == 0){
			echo "List ID is 0<br/>";
			$query = "INSERT INTO lists (list_name, for_fname, for_lname, description, location) VALUES(?, ?, ?, ?, ?)";
			if($stmt = $dbh->prepare($query)){
				echo "UID - $uid<br/>Name - $name <br/>fname - $fname<br/>lname - $lname<br/>description - $desc<br/>loc - $loc<br/>";
				$stmt->bind_param('sssss', $name, $fname, $lname, $desc, $loc);
				$stmt->execute();
				if($stmt->error != ""){
					echo $stmt->error;
				}else{
					echo "No error on insert<br/>";
					$stmt->close();
					$listid = $dbh->insert_id;//getListid(false);
					$arr = array('listid'=>$listid);
					if($listid != "0" || $listid != 0){
						echo "List ID is not 0 ($listid)<br/>";
						//update the lists_users table
						$query = "INSERT INTO lists_users (list_id, user_id) VALUES(?, ?)";

						if($stmt = $dbh->prepare($query)){
							$list += 0;
							$uid += 0;
							$stmt->bind_param('ii', $listid, $uid);
							$stmt->execute();
							if($stmt->error != ""){
								echo $stmt->error;
							}else{
								$stmt->close();
							}
						}else{
							//write email function to email support@theinfiniteregistry.com the error
							echo "unable to prepare stmt2<br/>";
						}
					}else
						echo "LISTID returned 0!!! List not added<br/>";
				}
			}else{
				echo $stmt->error;
				echo "<br/>".$dbh->error;
				//write email function to email support@theinfiniteregistry.com the error
				echo "<br/>unable to prepare stmt<br/>";
			}
		}else{
			$arr = array('listid'=>$listid);
		}
		
		$dbh->close();
		break;
}
echo "Array - $arr <br/><br/>";
$return = json_encode($arr);
echo $return;
?>
