<?php
include '../includes/db.php';

if(isset($_GET['type']))
	$type = escape($_GET['type']);
if(isset($_GET['uid']))
	$uid = escape($_GET['uid']);
else
	$uid = "0";
if(isset($_GET['listid']))
	$listid = escape($_GET['listid']);
else
	$listid = "0";
if(isset($_GET['upc']))
	$upc = escape($_GET['upc']);
else
	$upc = "0";

$return = "No return";
function escape($string){
	global $dbh;
	return $dbh->escape_string($string);
}

//Goes to factual db and get item details
//Insert findings into items table
function getItem(){
	global $dbh, $upc, $key, $secret;
	if($upc != "0"){
		require_once("../includes/factual/Factual.php");
		//OAuth to get the information
		$factual = new Factual($key, $secret);
		
		$query = new FactualQuery;
		$query->field("upc")->equal($upc);
		$res = $factual->fetch("products-cpg", $query);
		$resArr = $res->getData();
		$prod = $resArr[0]["brand"]." ".$resArr[0]["product_name"];
		$img = $resArr[0]["image_urls"][0];
		
		$query = "INSERT INTO items (upc, image, product) VALUES (?, ?, ?)";
		if(isset($dbh) && ($stmt = $dbh->prepare($query))){
			$stmt->bind_param('sss',$upc, $img, $prod);
			$stmt->execute();
		    $stmt->free_result();
			$stmt->error;
			$stmt->close();
		}else{
			//write email function to email support@theinfiniteregistry.com the error
			echo "unable to prepare stmt<br/>";
		}
	}else
		echo "No UPC found";
}

$arr = array();
switch($type){
	case "list":
		$query = "SELECT lst.list_id, lst.list_name FROM lists as lst, lists_users as usrs WHERE lst.list_id=usrs.list_id and usrs.user_id=?";
		if(isset($dbh) && ($stmt = $dbh->prepare($query))){
			$stmt->bind_param('i', $uid);
			$stmt->execute();
			
			$stmt->bind_result($listid, $listname);
			
			while($stmt->fetch()){
				$res = array("list_id"=>$listid, "list_name"=>$listname);
				array_push($arr, $res);
			}
		    $stmt->free_result();
			$stmt->error;
			$stmt->close();
		}else{
			//write email function to email support@theinfiniteregistry.com the error
			echo "unable to prepare stmt<br/>";
		}
		break;
	case "items":
		$query = "SELECT lst.list_name, COUNT(lst_itms.item_id), itms.product, itms.date, lst_itms.qty FROM items AS itms, lists AS lst, lists_items as lst_itms WHERE lst_itms.list_id=? AND lst.list_id=lst_itms.list_id and itms.item_id=lst_itms.item_id";
		if(isset($dbh) && ($stmt = $dbh->prepare($query))){
			$stmt->bind_param('i', $listid);
			$stmt->execute();
			
			$stmt->bind_result($name, $count, $product, $date, $qty);
			$loopCtr=0;
			while($stmt->fetch()){
				if($loopCtr==0){
					$temp = array("name"=>$name);
					array_push($arr, $temp);
					$temp = array("count"=>$count);
					array_push($arr, $temp);
				}
				$loopCtr++;
				$res = array("product"=>$product, "date"=>$date, "qty"=> $qty);
				array_push($arr, $res);
			}
		    $stmt->free_result();
			$stmt->error;
			$stmt->close();
		}else{
			//write email function to email support@theinfiniteregistry.com the error
			echo "unable to prepare stmt<br/>";
		}
		break;
	case "scan":
		$query = "SELECT count(*) FROM items WHERE upc=?";
		if(isset($dbh) && ($stmt = $dbh->prepare($query))){
			$stmt->bind_param('s', $upc);
			$stmt->execute();			
			$stmt->bind_result($count);
			$stmt->fetch();
			$stmt->free_result();
			$stmt->error;
			$stmt->close();
			
			if($count < 1)
				getItem();
				
			$query = "SELECT item_id, product FROM items WHERE upc=?";
			if(isset($dbh) && ($stmt = $dbh->prepare($query))){
				$stmt->bind_param('s', $upc);
				$stmt->execute();			
				$stmt->bind_result($item_id, $product);
				$stmt->fetch();
				$res = array("item_id"=>$item_id, "product"=>$product);
				array_push($arr, $res);
				$stmt->free_result();
				$stmt->error;
				$stmt->close();
			}else{
				//write email function to email support@theinfiniteregistry.com the error
				echo "unable to prepare stmt<br/>";
			}
		}else{
			//write email function to email support@theinfiniteregistry.com the error
			echo "unable to prepare stmt<br/>";
		}
		break;
}
$return =json_encode($arr);

echo $return;
?>
