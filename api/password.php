<?php
include '../includes/db.php' ;

if(isset($_POST['email'])){
	$email = urldecode($_POST['email']); //test - josh.richard1@gmail.com
}
if(isset($_POST['password'])){
	$password = urldecode($_POST['password']); //test - Password1!
}

if(!isset($email) && !isset($password)){
	echo json_encode(array('data'=>array(), 'info'=>'No email or password', 'status'=>403));
	die;
}

$query = "SELECT user_id, password FROM user_login_t WHERE email=?";
if($sth = $dbh->prepare($query)){
    $sth->bind_param('s', $email);
    $sth->execute();
    $sth->bind_result($uid, $stored_pass);
    $sth->fetch();
    $sth->free_result();
    $sth->close();

    if(crypt($password, $stored_pass) === $stored_pass){
        $arr = array('data'=>array('uid'=>$uid), 'status'=>200);
    }else{
        $arr = array('data'=>array(), 'info'=>array('message'=>'Login not found', 'email'=>$email, 'password'=>$password), 'status'=>403);
    }
} else {
    $arr = array('data'=>array($dbh->errno), 'info'=>array($dbh->error), 'status'=>503);
}

$dbh->close();
echo json_encode($arr);

die;
?>
