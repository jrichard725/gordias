CREATE DATABASE infinite_registry;

CREATE USER 'infinite_admin'@'localhost' IDENTIFIED BY 'L1stM32014';
CREATE TABLE user_login_t(
    user_id INT(10),
    email VARCHAR(100),
    password VARCHAR(255)
);

GRANT ALL on infinite_registry.* TO 'infinite_admin';
